import {createRouter, createWebHistory} from 'vue-router'
import Login from '@/views/manager/Login.vue'
import Manager from '@/views/manager/Manager.vue'
import NotFound from '@/views/404.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: '登录'
      }
    },
    {
      path: '/manager',
      name: 'manager',
      component: Manager,
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'home',
          component: () => import('@/views/manager/Home.vue'),
          meta: {
            title: '首页'
          }
        },
        {
          path: '/personal',
          name: 'personal',
          component: () => import('@/views/manager/Personal.vue'),
          meta: {
            title: '个人中心'
          }
        },
        {
          path: '/notice',
          name: 'notice',
          component: () => import('@/views/manager/Notice.vue'),
          meta: {
            title: '教务公告'
          }
        },
        {
          path: '/admin',
          name: 'admin',
          component: () => import('@/views/manager/Admin.vue'),
          meta: {
            title: '用户列表'
          }
        },
        {
          path: '/teacher',
          name: 'teacher',
          component: () => import('@/views/manager/Teacher.vue'),
          meta: {
            title: '教师列表'
          }
        },
        {
          path: '/speciality',
          name: 'speciality',
          component: () => import('@/views/manager/Speciality.vue'),
          meta: {
            title: '专业列表'
          }
        },
        {
          path: '/classes',
          name: 'classes',
          component: () => import('@/views/manager/Classes.vue'),
          meta: {
            title: '班级列表'
          }
        },
        {
          path: '/student',
          name: 'student',
          component: () => import('@/views/manager/Student.vue'),
          meta: {
            title: '学生列表'
          }
        }
      ]
    },
    {
        path: '/:pathMatch(.*)',
        name: 'NotFound',
        component: NotFound,
        meta: {
            title: '404'
        }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})

export default router
