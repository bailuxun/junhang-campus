import { createApp } from 'vue'
// 导入pinia数据存储插件
import { createPinia } from 'pinia'
// 导入pinia数据持久化插件
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import App from './App.vue'
import router from './router'
// 导入ElementPlus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 导入ElementPlus icon图标
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
//引入element-plus中文包
import zhCn from 'element-plus/dist/locale/zh-cn.mjs' 

const app = createApp(App)

const pinia  = createPinia()

app.use(pinia.use(piniaPluginPersistedstate))
app.use(router)

// 实例化ElementPlus
app.use(ElementPlus,{ locale: zhCn })

// 循环注册图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

app.mount('#app')
// main.ts

