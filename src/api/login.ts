// 导入请求方法
import request from '@/utils/axios'

// 导入leementpuls的消息提示组件
import { ElMessage } from 'element-plus'

// 登陆请求
export const Login = async (params: object) => {
    let { code, data, msg } = await request.post('/login', params);
    if (code === '200') {
        // 保存用户tokn到浏览器
        sessionStorage.setItem('token', data.token)
        // 提示消息
        ElMessage.success(msg)
        return { data, 'status': true }
    } else {
        // 提示消息
        ElMessage.error(msg)
        return { data, 'status': false }
    }
}
