// 使用pinia保存用户信息
import { defineStore } from 'pinia'

export default defineStore('user', {
    state() {
        return {
            user: {}
        }
    },
    actions: {
        setUser(user:object) {
            this.user = user
        }
    },
    // 数据持久化
    persist: {
        enabled: true,
    }
})