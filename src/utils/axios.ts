// 导入axios
import axios from 'axios'

//导入配置文件
import {baseURL_dev} from '@/config/config'


//初始化一个axios对象
const instance = axios.create({
    // 基础url
    baseURL: baseURL_dev,
    // 超时时间
    timeout: 10000,
})

// 请求拦截
// 可以自请求发送前对请求做一些处理
// 比如统一加token，对请求参数统一加密
instance.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';        // 设置请求头格式
    let user = JSON.parse(localStorage.getItem("xm-user") || '{}')  // 获取缓存的用户信息
    config.headers['token'] = user.token  // 设置请求头

    return config
}, error => {
    console.error('request error: ' + error) // for debug
    return Promise.reject(error)
});

// 响应拦截
instance.interceptors.response.use(
    response => {
        // 兼容服务端返回的字符串数据
        if (typeof response.data ==='string') {
            response.data = response.data? JSON.parse(response.data) : response.data
        }
        let data = response.data
        // 判断是否有权限
        // if (res.code === '401') {
        //     router.push('/login')
        // }
        return data
    },
    error => {
        console.error('response error: ' + error) // for debug
        return Promise.reject(error)
    }
)

export default instance
