// 导入登录的用户信息
// import user from '@/stores/user'
// // 用户头像
// let userStore = user()
// console.log(userStore)

// 左侧全部菜单列表
export const menu = [
    {
        id: 1,
        name: "信息公告",
        path: "affiche",
        children: [
            {
                id: 101,
                name: "教务通告",
                path: "notice"
            },
            {
                id: 102,
                name: "考试安排",
                path: "examPlan"
            },
            {
                id: 103,
                name: "教室安排",
                path: "roomPlan"
            }
        ]
    },
    {
        id: 2,
        name: "行政管理",
        path: "administration",
        children: [
            {
                id: 101,
                name: "学院管理",
                path: "college"
            },
            {
                id: 102,
                name: "专业信息",
                path: "speciality"
            },
            {
                id: 103,
                name: "班级信息",
                path: "classes"
            }
        ]
    },
    {
        id: 3,
        name: "教学管理",
        path: "teaching",
        children: [
            {
                id: 301,
                name: "课程信息",
                path: "course"
            },
            {
                id: 302,
                name: "我的选课",
                path: "choice"
            },
            {
                id: 303,
                name: "我的课表",
                path: "curriculum"
            },
            {
                id: 304,
                name: "我的成绩",
                path: "score"
            },
            {
                id: 305,
                name: "网上教评",
                path: "comment"
            }
        ]
    },
    {
        id: 4,
        name: "教务管理",
        path: "educational",
        children: [
            {
                id: 401,
                name: "请假申请",
                path: "apply"
            },
            {
                id: 402,
                name: "作业提交",
                path: "homework"
            },
            {
                id: 403,
                name: "考勤信息",
                path: "attendance"
            },
            {
                id: 404,
                name: "教案信息",
                path: "teachingPlan"
            }
        ]
    },
    {
        id: 5,
        name: "用户管理",
        path: "users",
        children: [
            {
                id: 501,
                name: "管理员信息",
                path: "admin"
            },
            {
                id: 502,
                name: "教师信息",
                path: "teacher"
            },
            {
                id: 503,
                name: "学生信息",
                path: "student"
            }
        ]
    }
]

// 管理员的左侧菜单列表
export const adminMenu = [
    {
        id: 1,
        name: "信息公告",
        path: "affiche",
        children: [
            {
                id: 101,
                name: "教务通告",
                path: "notice"
            },
            {
                id: 102,
                name: "考试安排",
                path: "examPlan"
            },
            {
                id: 303,
                name: "教室安排",
                path: "roomPlan"
            }
        ]
    },
    {
        id: 2,
        name: "行政管理",
        path: "administration",
        children: [
            {
                id: 101,
                name: "学院管理",
                path: "college"
            },
            {
                id: 102,
                name: "专业信息",
                path: "speciality"
            },
            {
                id: 103,
                name: "班级信息",
                path: "classes"
            }
        ]
    },
    {
        id: 3,
        name: "教学管理",
        path: "teaching",
        children: [
            {
                id: 301,
                name: "课程信息",
                path: "course"
            },
            {
                id: 302,
                name: "查看选课",
                path: "choice"
            },
            {
                id: 303,
                name: "我的课表",
                path: "curriculum"
            },
            {
                id: 304,
                name: "查看成绩",
                path: "score"
            },
            {
                id: 305,
                name: "网上教评",
                path: "comment"
            }
        ]
    },
    {
        id: 4,
        name: "教务管理",
        path: "educational",
        children: [
            {
                id: 401,
                name: "请假申请",
                path: "apply"
            },
            {
                id: 402,
                name: "查看作业",
                path: "homework"
            },
            {
                id: 403,
                name: "考勤信息",
                path: "attendance"
            },
            {
                id: 404,
                name: "教案信息",
                path: "teachingPlan"
            }
        ]
    },
    {
        id: 5,
        name: "用户管理",
        path: "users",
        children: [
            {
                id: 501,
                name: "管理员信息",
                path: "admin"
            },
            {
                id: 502,
                name: "教师信息",
                path: "teacher"
            },
            {
                id: 503,
                name: "学生信息",
                path: "student"
            }
        ]
    }
]

// 教师的左侧菜单
export const teacherMenu = [
    {
        id: 1,
        name: "信息公告",
        path: "affiche",
        children: [
            {
                id: 101,
                name: "教务通告",
                path: "notice"
            },
            {
                id: 102,
                name: "考试安排",
                path: "examPlan"
            },
            {
                id: 103,
                name: "教室安排",
                path: "roomPlan"
            }
        ]
    },
    {
        id: 2,
        name: "行政管理",
        path: "administration",
        children: [
            {
                id: 101,
                name: "学院管理",
                path: "college"
            },
            {
                id: 102,
                name: "专业信息",
                path: "speciality"
            },
            {
                id: 103,
                name: "班级信息",
                path: "classes"
            }
        ]
    },
    {
        id: 3,
        name: "教学管理",
        path: "teaching",
        children: [
            {
                id: 301,
                name: "课程信息",
                path: "course"
            },
            {
                id: 302,
                name: "查看选课",
                path: "choice"
            },
            {
                id: 303,
                name: "我的课表",
                path: "curriculum"
            },
            {
                id: 304,
                name: "查看成绩",
                path: "score"
            },
            {
                id: 305,
                name: "我的教评",
                path: "comment"
            }
        ]
    },
    {
        id: 4,
        name: "教务管理",
        path: "educational",
        children: [
            {
                id: 402,
                name: "查看提交",
                path: "homework"
            },
            {
                id: 403,
                name: "考勤信息",
                path: "attendance"
            },
            {
                id: 404,
                name: "教案信息",
                path: "teachingPlan"
            }
        ]
    },
    {
        id: 5,
        name: "用户管理",
        path: "users",
        children: [
            {
                id: 503,
                name: "学生信息",
                path: "student"
            }
        ]
    }
]

// 学生的左侧菜单
export const studentMenu = [
    {
        id: 1,
        name: "信息公告",
        path: "affiche",
        children: [
            {
                id: 503,
                name: "教室安排",
                path: "roomPlan"
            }
        ]
    },
    {
        id: 3,
        name: "教学管理",
        path: "teaching",
        children: [
            {
                id: 301,
                name: "课程信息",
                path: "course"
            },
            {
                id: 302,
                name: "我的选课",
                path: "choice"
            },
            {
                id: 303,
                name: "我的课表",
                path: "curriculum"
            },
            {
                id: 304,
                name: "我的成绩",
                path: "score"
            },
            {
                id: 305,
                name: "网上教评",
                path: "comment"
            }
        ]
    },
    {
        id: 4,
        name: "教务管理",
        path: "educational",
        children: [
            {
                id: 401,
                name: "请假申请",
                path: "apply"
            },
            {
                id: 402,
                name: "作业提交",
                path: "homework"
            },
            {
                id: 403,
                name: "考勤信息",
                path: "attendance"
            }
        ]
    }
]