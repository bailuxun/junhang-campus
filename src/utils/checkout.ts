/**
 * 验证用户输入的用户名
 * @param rule - 规则对象
 * @param value - 输入的密码
 * @param callback - 回调函数
 */
export const validateuser = (rule: any, value: any, callback: any) => {
    // 去除空格
    value = value.replace(/\s/g, '')
    if (value == '') {
        // 如果密码为空，则提示用户输入账号
        callback(new Error('请输入用户名'))
    } else if (!user.test(value)) {
        callback(new Error('请输入5-20位的账号'))
    }
    // 回调函数，表示密码验证通过
    callback()
}

/**
 * 验证用户输入的姓名
 * @param rule - 规则对象
 * @param value - 输入的姓名
 * @param callback - 回调函数
 */
export const validatename = (rule: any, value: any, callback: any) => {
    // 去除空格
    value = value.replace(/\s/g, '')
    if (value == '') {
        // 如果密码为空，则提示用户输入账号
        callback(new Error('请输入姓名'))
    } else {
        // 回调函数，表示账号验证通过
        callback()
    }
}

/**
 * 验证用户输入的密码
 * @param rule - 规则对象
 * @param value - 输入的用户名
 * @param callback - 回调函数
 */
export const validatePass = (rule: any, value: any, callback: any) => {
    // 去除空格
    value = value.replace(/\s/g, '')
    // 验证密码是否为空
    if (value == '') {
        callback(new Error('请输入6-16位且包含大小写字母、数字和特殊字符的密码'))
    } else if (!pwd.test(value)) {
        callback(new Error('密码必须为6-16位且包含大小写字母、数字和特殊字符'))
    }
    // 回调函数，表示密码验证通过
    callback()
}

/**
 * 验证用户输入的手机号码
 * @param rule - 规则对象
 * @param value - 输入的手机号码
 * @param callback - 回调函数
 */
export const validatemobile = (rule: any, value: any, callback: any) => {
    // 去除空格
    value = value.replace(/\s/g, '')
    // 验证密码是否为空
    if (value == '') {
        callback(new Error('请输手机号码'))
    } else {
        if (!mobile.test(value)) {
            callback(new Error('请输入正确的手机号码'))
        }
        // 回调函数，表示密码验证通过
        callback()
    }
}

/**
 * 验证用户输入的邮箱
 * @param rule - 规则对象
 * @param value - 输入的邮箱
 * @param callback - 回调函数
 */
export const validateEmail = (rule: any, value: any, callback: any) => {
    // 去除空格
    value = value.replace(/\s/g, '')
    // 验证密码是否为空
    if (value == '') {
        callback(new Error('请输邮箱'))
    } else {
        if (!email.test(value)) {
            callback(new Error('请输入正确的邮箱'))
        }
        // 回调函数，表示密码验证通过
        callback()
    }
}

/*
* 用户名正则
* ^：匹配输入的开始
* [a-zA-Z0-9_-]：配由大小写字母、数字、下划线和中划线组成的字符串
* {5,20}：字符串的长度在5到20之间
*/
const user = /^[a-zA-Z0-9_-]{5,20}$/;

/*
* 密码校验正则
* ^：匹配输入的开始
* (?=.*[a-z])：至少包含一个小写字母
* (?=.*[A-Z])：至少包含一个大写字母
* (?=.*\d)：至少包含一个数字
* (?=.*[@$!%*?&])：至少包含一个特殊字符（可以根据需求修改特殊字符的列表）
* [A-Za-z\d@$!%*?&]+：匹配包含大小写字母、数字和特殊字符的字符串
* {6,16}：字符串的长度在6到16之间
* $：匹配输入的结束
*/
const pwd = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,16}$/;

/*
* 手机号码正则
* ^：匹配输入的开始
* [1]：匹配1
* [3-9]：匹配3到9之间的数字
* \d{9}：匹配9个数字
* $：匹配输入的结束
*/
const mobile = /^[1][3-9]\d{9}$/;


/*
* 邮箱正则
* ^：匹配输入的开始
* [a-zA-Z0-9_-]：配由大小写字母、数字、下划线和中划线组成的字符串
* +@[a-zA-Z0-9_-]：匹配@符号后跟的
* +(\.[a-zA-Z0-9_-]+)：匹配用户名后面的域名
* $：匹配输入的结束
*/
const email = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;

/*
* 身份证正则
* ^：匹配输入的开始
* (^[1-9]\d{5}(18|19|20): 匹配18位身份证
* \d{2}: 匹配出生日期的年份
* ([01]\d|2[0-4]|25): 匹配出生日期的月份
* (0[1-9]|[12]\d|3[01])\d{3}[0-9Xx]$): 匹配出生日期的天数和校验位
* |(^[1-9]\d{5}([0-9]|X)[0-9]$): 匹配15位身份证
*/
const identity = /^(^[1-9]\d{5}(18|19|20)\d{2}([01]\d|2[0-4]|25)(0[1-9]|[12]\d|3[01])\d{3}[0-9Xx]$)|(^[1-9]\d{5}([0-9]|X)[0-9]$)/;
