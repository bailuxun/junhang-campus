import {fileURLToPath, URL} from 'node:url'

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
    ],
    server: {
        host: true,
        // 设置端口号
        port: 8081, //自动打开浏览器
        open: true,
        proxy: {
            "/api": { // 代理目标地址
                target: "http://127.0.0.1", // 允许跨域
                changeOrigin: true, // 开启websockets代理
                ws: false, // 验证SSL证书
                secure: false, // 重写path路径
                rewrite: (path) => path.replace(/^\/api/, ""),
            },
        },
    },
    resolve: {
        alias: {
            '@':
                fileURLToPath(new URL('./src', import.meta.url))
        }
    }
})
